﻿using System;

namespace week_3_exercise_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, please enter a number");

            var num1 = int.Parse(Console.ReadLine());

            Console.WriteLine($"Below are the multiplications for your number from 1 to 12.");
            Console.WriteLine($"Multiplied by 1 is {num1 * 1}");
            Console.WriteLine($"Multiplied by 2 is {num1 * 2}");
            Console.WriteLine($"Multiplied by 3 is {num1 * 3}");
            Console.WriteLine($"Multiplied by 4 is {num1 * 4}");
            Console.WriteLine($"Multiplied by 5 is {num1 * 5}");
            Console.WriteLine($"Multiplied by 6 is {num1 * 6}");
            Console.WriteLine($"Multiplied by 7 is {num1 * 7}");
            Console.WriteLine($"Multiplied by 8 is {num1 * 8}");
            Console.WriteLine($"Multiplied by 9 is {num1 * 9}");
            Console.WriteLine($"Multiplied by 10 is {num1 * 10}");
            Console.WriteLine($"Multiplied by 11 is {num1 * 11}");
            Console.WriteLine($"Multiplied by 12 is {num1 * 12}");
                        
        }
    }
}
       